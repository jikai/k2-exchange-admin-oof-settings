# K2-Exchange Admin OOF Broker #

This broker uses PowerShell to query Exchange on the OOF user list and update K2 users’ OOF status.

### Limitations ###

* The broker uses Kerberos authentication to interact with Exchange’s WinRM. Kerberos is the default authentication mode for WinRM.
* The broker can only update the OOF status on the K2 user account. It does not manage the forwarding and exception rules.
* The broker can only see the list of K2 user accounts that has an existing forwarding/exception rule. This list of accounts can be found in the K2 Workspace.
* The broker has been tested with Service Account, Impersonate, SSO and Static Authentication Modes. It has not been tested with OAuth and shall be assumed to be not working.

### How do I get set up? ###

The deployment guides are found in the Deploy folder of the package