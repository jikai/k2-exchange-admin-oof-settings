﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using SourceCode.SmartObjects.Services.ServiceSDK.Objects;
using Attributes = SourceCode.SmartObjects.Services.ServiceSDK.Attributes;
using SourceCode.SmartObjects.Services.ServiceSDK.Types;
using System.Security;
using System.Management.Automation.Runspaces;
using System.Management.Automation.Host;
using System.Management.Automation;
using System.Collections.ObjectModel;
using System.Collections;
using SCWFM = SourceCode.Workflow.Management;
using System.Configuration;

namespace K2ExchangeAdminOOFBroker.Data
{
    /// <summary>
    /// Sample implementation of a Static Service Object.
    /// The class is decorated with a ServiceObject Attribute providing definition information for the Service Object.
    /// The Properties and Methods in the class are each decorated with attributes that describe them in Service Object terms
    /// This sample implementation contains two Properties (Number and Text) and two Methods (Read and List)
    /// </summary>
    [Attributes.ServiceObject("ExchangeAdminOOF", "ExchangeAdminOOF", "Get Exchange OOF users")]
    class ExchangeAdminOOF
    {
	
		/// <summary>
        /// This property is required if you want to get the service instance configuration 
        /// settings in this class
        /// </summary>
        private ServiceConfiguration _serviceConfig;
        public ServiceConfiguration ServiceConfiguration
        {
            get { return _serviceConfig; }
            set { _serviceConfig = value; }
        }
		
        #region Class Level Fields

        private const string ConnectionUriFormat = "http://{0}/PowerShell";
        private Runspace runspace = null;


        #region Private Fields
        private string samAccountName;
        private string emailAddress;
        private DateTime startDateTime;
        private DateTime endDateTime;
        private string internalMessage;
        private string externalAudience; //None, All or Known
        private string externalMessage;
        private string autoReplyState;
        private string distinguishedName; //CN = ...
        private string identity;
        private string syncStatus; // message for sync only.
        private string fqn;
        #endregion

        #endregion

        #region Properties with Property Attribute
        [Attributes.Property("FQN", SoType.Text, "K2 FQN", "K2 FQN")]
        public string FQN
        {
            get { return fqn; }
            set { fqn = value; }
        }

        [Attributes.Property("SyncStatus", SoType.Text, "Sync Status", "Exchange to K2 OOF Sync Status")]
        public string SyncStatus
        {
            get { return syncStatus; }
            set { syncStatus = value; }
        }

        [Attributes.Property("DistinguishedName", SoType.Text, "Distinguished Name", "Distinguished Name")]
        public string DistinguishedName
        {
            get { return distinguishedName; }
            set { distinguishedName = value; }
        }

        [Attributes.Property("Identity", SoType.Text, "Identity", "Identity")]
        public string Identity
        {
            get { return identity; }
            set { identity = value; }
        }

        [Attributes.Property("SamAccountName", SoType.Text, "Login Name", "Ad Login Name")]
        public string SamAccountName
        {
            get { return samAccountName; }
            set { samAccountName = value; }
        }

        [Attributes.Property("EmailAddress", SoType.Text, "Email", "Email")]
        public string EmailAddress
        {
            get { return emailAddress; }
            set { emailAddress = value; }
        }

        [Attributes.Property("StartDateTime", SoType.DateTime, "Start Date/Time", "Start Date/Time")]
        public DateTime StartDateTime
        {
            get { return startDateTime; }
            set { startDateTime = value; }
        }

        [Attributes.Property("EndDateTime", SoType.DateTime, "End Date/Time", "End Date/Time")]
        public DateTime EndDateTime
        {
            get { return endDateTime; }
            set { endDateTime = value; }
        }

        [Attributes.Property("InternalMessage", SoType.Text, "Internal Message", "Internal Email Message")]
        public string InternalMessage
        {
            get { return internalMessage; }
            set { internalMessage = value; }
        }

        [Attributes.Property("ExternalAudience", SoType.Text, "External Audience", "External Audience = None, All or Known")]
        public string ExternalAudience
        {
            get { return externalAudience; }
            set { externalAudience = value; }
        }

        [Attributes.Property("ExternalMessage", SoType.Text, "External Message", "External Email Message")]
        public string ExternalMessage
        {
            get { return externalMessage; }
            set { externalMessage = value; }
        }

        [Attributes.Property("AutoReplyState", SoType.Text, "Auto Reply State", "Auto Reply State")]
        public string AutoReplyState
        {
            get { return autoReplyState; }
            set { autoReplyState = value; }
        }

        #endregion

        #region Default Constructor
        /// <summary>
        /// Instantiates a new ServiceObject1.
        /// </summary>
        public ExchangeAdminOOF()
        {
            // No implementation necessary.
        }
        #endregion

        #region Methods with Method Attribute

        #region List<ExchangeOOF> GetOOFUsers
        /// <summary>
        /// Get OOF Users list
        /// </summary>
        [Attributes.Method("GetOOFUsers", MethodType.List, "Get OOF Users", "Get OOF Users", 
            new string[] { },  //required property array 
            new string[] { }, //input property array 
            new string[] { "SamAccountName", "EmailAddress", "StartDateTime", "EndDateTime", "InternalMessage", "ExternalAudience", "ExternalMessage", "AutoReplyState" })] //return property array 
        public List<ExchangeAdminOOF> GetOOFUsers()
        {
            List<ExchangeAdminOOF> result = new List<ExchangeAdminOOF>();
            //List<PSResult> psResult = this.ExecuteCmd("Get-Mailbox | Get-MailboxAutoReplyConfiguration | Where-Object { $_.AutoReplyState -ne \"disabled\" }", true);

            PSObject session = this.OpenConnection();
            try
            {
                //find all mailbox where the "AutoReplyState" = Enabled or Scheduled.
                Collection<PSObject> psAutoReplyEnabled = this.ExecutePowerShellCmd(this.GetOOFPSCommand());
                // Get OOF user details
                Collection<PSObject> psUserDetails = this.ExecutePowerShellCmd(this.GetOOFUserDetails());

                foreach(PSObject oof in psAutoReplyEnabled)
                {
                    ExchangeAdminOOF obj = new ExchangeAdminOOF();
                    obj.AutoReplyState = oof.Properties["AutoReplyState"].Value.ToString();
                    obj.StartDateTime = Convert.ToDateTime(oof.Properties["StartTime"].Value);
                    obj.EndDateTime = Convert.ToDateTime(oof.Properties["EndTime"].Value);
                    obj.InternalMessage = oof.Properties["InternalMessage"].Value.ToString();
                    obj.ExternalAudience = oof.Properties["ExternalAudience"].Value.ToString();
                    obj.ExternalMessage = oof.Properties["ExternalMessage"].Value.ToString();
                    obj.Identity = oof.Properties["Identity"].Value.ToString();

                    PSObject userDetail = psUserDetails.Single(u => u.Properties["Identity"].Value.ToString() == obj.Identity);
                    obj.SamAccountName = userDetail.Properties["SamAccountName"].Value.ToString();
                    //can't find a good way to just extrac the email string from the multi-valued property
                    obj.EmailAddress = userDetail.Properties["EmailAddresses"].Value.ToString().Replace("SMTP:", string.Empty); 
                    obj.DistinguishedName = userDetail.Properties["DistinguishedName"].Value.ToString();
                    
                    result.Add(obj);
                }
            }
            finally
            {
                this.CloseConnection(session);
            }
            return result;
        }   
        #endregion

        #region List<ExchangeOOF> SyncOOFToK2
        /// <summary>
        /// Get OOF Users list
        /// </summary>
        [Attributes.Method("SyncOOFToK2", MethodType.List, "Sync OOF to K2", "Sync Exchange OOF to K2", 
            new string[] { },  //required property array 
            new string[] { }, //input property array 
            new string[] { "FQN", "SamAccountName", "SyncStatus"})] //return property array 
        public List<ExchangeAdminOOF> SyncOOFToK2()
        {
            List<ExchangeAdminOOF> result = new List<ExchangeAdminOOF>();
            SCWFM.WorkflowManagementServer K2WfSvr = new SCWFM.WorkflowManagementServer();
            PSObject session = this.OpenConnection();
            try
            {
                // Get OOF user details
                Collection<PSObject> psUserDetails = this.ExecutePowerShellCmd(this.GetOOFUserDetails());
                // open workflow server connection
                K2WfSvr.Open(ConfigurationManager.ConnectionStrings["WorkflowServer"].ConnectionString);
                // This will get all users who have OOF settings in K2. Look at K2 Workspace > Management Console > Workflow Server > Users > Out Of office.
                SCWFM.OOF.Users oofK2Users = K2WfSvr.GetUsers(SCWFM.ShareType.OOF);
                // check the K2 users who are OOF and set to available if not OOF in exchange
                foreach(SCWFM.OOF.User K2User in oofK2Users.Where(u => u.Status == SCWFM.UserStatuses.OOF))
                {
                    string loginName = this.GetLoginNameFromK2FQN(K2User.FQN).ToLower();
                    bool found = psUserDetails.Any(ps => ps.Properties["SamAccountName"].Value.ToString().ToLower() == loginName);
                    if(!found)
                    { //if not found, set status to available
                        K2WfSvr.SetUserStatus(K2User.FQN, SCWFM.UserStatuses.Available);
                        #region write object
                        ExchangeAdminOOF obj = new ExchangeAdminOOF();
                        obj.FQN = K2User.FQN;
                        obj.SamAccountName = loginName;
                        obj.SyncStatus = string.Format("OOF Status: {0} => {1}", K2User.Status.ToString(), SCWFM.UserStatuses.Available.ToString());
                        result.Add(obj);
                        #endregion
                    }
                }
                // check the powershell list for any oof user who is "available" or "none" in K2 OOF now
                foreach (PSObject user in psUserDetails)
                {
                    string K2FQN = this.FormatK2FQN(user.Properties["SamAccountName"].Value.ToString()).ToLower();
                    bool found = oofK2Users.Any(u => u.FQN.ToLower() == K2FQN && u.Status != SCWFM.UserStatuses.OOF);
                    if (found)
                    {   //Set to OOF
                        K2WfSvr.SetUserStatus(K2FQN, SCWFM.UserStatuses.OOF);
                        #region write object
                        ExchangeAdminOOF obj = new ExchangeAdminOOF();
                        obj.FQN = K2FQN;
                        obj.SamAccountName = user.Properties["SamAccountName"].Value.ToString();
                        obj.SyncStatus = string.Format("OOF Status: {0} => {1}", SCWFM.UserStatuses.Available.ToString(), SCWFM.UserStatuses.OOF.ToString());
                        result.Add(obj);
                        #endregion
                    }
                }
                // print the users who did not setup K2 OOF. The code cannot help these user setup the K2 OOF status
                List<PSObject> usersNotInK2OOF = psUserDetails.Where(ps => !oofK2Users.Any(k2 => this.GetLoginNameFromK2FQN(k2.FQN).ToLower() == ps.Properties["SamAccountName"].Value.ToString().ToLower())).ToList();
                foreach(PSObject user in usersNotInK2OOF)
                {
                    #region write object
                    ExchangeAdminOOF obj = new ExchangeAdminOOF();
                    obj.SamAccountName = user.Properties["SamAccountName"].Value.ToString();
                    obj.SyncStatus = string.Format("No OOF settings in K2");
                    result.Add(obj);
                    #endregion
                }
            }
            finally
            {
                if (K2WfSvr.Connection != null && K2WfSvr.Connection.IsConnected)
                {
                    K2WfSvr.Connection.Dispose();
                }
                this.CloseConnection(session);
            }

            return result;
        }

        #endregion

        #endregion

        #region private
        private string FormatK2FQN(string loginName)
        {
            string securityLabel = this.ServiceConfiguration["SecurityLabel"].ToString();
            string domainName = this.ServiceConfiguration["DomainName"].ToString();
            return string.Format("{0}:{1}\\{2}", securityLabel, domainName, loginName);
        }
        private string GetLoginNameFromK2FQN(string fqn)
        {
            return fqn.Split(new string[] { "\\" }, StringSplitOptions.None)[1];
        }
        private SecureString SecurePassword(string password)
        {
            if (string.IsNullOrEmpty(password.Trim()))
            {
                return null;
            }
            else
            {
                SecureString psPwd = new SecureString();
                foreach (char c in password)
                {
                    psPwd.AppendChar(c);
                }
                psPwd.MakeReadOnly();
                return psPwd;
            }
        }
        private PSCommand GetOOFPSCommand()
        {
            //"Get-Mailbox | Get-MailboxAutoReplyConfiguration | Where-Object { $_.AutoReplyState -ne \"disabled\" }";
            PSCommand cmd = new PSCommand();
            cmd.AddCommand("Get-Mailbox");
            cmd.AddCommand("Get-MailboxAutoReplyConfiguration");

            Command whereCmd = new Command("Where-Object");
            whereCmd.Parameters.Add("FilterScript", ScriptBlock.Create("$_.AutoReplyState -ne 'disabled'"));
            cmd.AddCommand(whereCmd);

            return cmd;
        }
        private PSCommand GetOOFUserDetails()
        {
            PSCommand cmd = this.GetOOFPSCommand();

            cmd.AddCommand("Get-Mailbox"); // | Get-Mailbox

            List<string> userProp = new List<string>() { "SamAccountName", "Identity", "DistinguishedName", "EmailAddresses" };
            Command cmdSelectUserDetails = new Command("Select-Object"); // | select
            cmdSelectUserDetails.Parameters.Add("Property", userProp.ToArray());
            cmd.AddCommand(cmdSelectUserDetails);

            //This doesn't work.
            //userProp.Add("SmtpAddress");
            //Command cmdSmtpAddress = new Command("Select-Object"); //  select
            //cmdSmtpAddress.Parameters.Add("Property", userProp.ToArray());
            //cmd.AddCommand(cmdSmtpAddress);

            return cmd;
        }
        private Uri GetConnectionUri()
        {
            Uri result = null;
            try
            {
                result = new Uri(string.Format(ConnectionUriFormat, this.ServiceConfiguration["ExchangeServerURI"]));
            }
            catch(Exception ex)
            {
                throw new Exception("Unable to format the Exchange server's URI. " + ex.Message, ex);
            }
            return result;
        }
        private PSObject OpenConnection()
        {
            //http://stackoverflow.com/questions/31381668/using-powershell-calculated-properties-from-c-sharp
            runspace = RunspaceFactory.CreateRunspace();
            runspace.Open();

            PSObject psSessionConnection;

            // Create a powershell session for remote exchange server
            var newSessionCmd = new PSCommand();
            newSessionCmd.AddCommand("New-PSSession");
            newSessionCmd.AddParameter("ConfigurationName", "Microsoft.Exchange");
            newSessionCmd.AddParameter("ConnectionUri", this.GetConnectionUri());
            newSessionCmd.AddParameter("Authentication", "Kerberos");
            if (this.ServiceConfiguration.ServiceAuthentication.AuthenticationMode == AuthenticationMode.SSO ||
                    this.ServiceConfiguration.ServiceAuthentication.AuthenticationMode == AuthenticationMode.Static)
            {
                PSCredential cred = new PSCredential(
                    this.ServiceConfiguration.ServiceAuthentication.UserName,
                    SecurePassword(this.ServiceConfiguration.ServiceAuthentication.Password));

                newSessionCmd.AddParameter("Credential", cred);
            }
            // TODO: Handle errors
            Collection<PSObject> result = this.ExecutePowerShellCmd(newSessionCmd);
            psSessionConnection = result[0];

            try
            {
                // Set ExecutionPolicy on the process to unrestricted
                PSCommand policyCmd = new PSCommand();
                policyCmd.AddCommand("Set-ExecutionPolicy");
                policyCmd.AddParameter("Scope", "Process");
                policyCmd.AddParameter("ExecutionPolicy", "Unrestricted");
                this.ExecutePowerShellCmd(policyCmd);
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to set Execution Policy. " + ex.Message, ex);
            }

            try
            { 
                // Import remote exchange session into runspace
                PSCommand importCmd = new PSCommand();
                importCmd.AddCommand("Import-PSSession");
                importCmd.AddParameter("Session", psSessionConnection);
                this.ExecutePowerShellCmd(importCmd);
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to import PSSession. " + ex.Message, ex);
            }

            return psSessionConnection;
        }
        private void CloseConnection(PSObject psSessionConnection)
        {
            PSCommand command = new PSCommand();
            command.AddCommand("Remove-PSSession");
            command.AddParameter("Session", psSessionConnection);
            this.ExecutePowerShellCmd(command);
            runspace.Close();
        }
        private Collection<PSObject> ExecutePowerShellCmd(PSCommand cmd)
        {
            Collection<PSObject> result = null;
            using (PowerShell powershell = PowerShell.Create())
            {
                powershell.Commands = cmd;
                powershell.Runspace = runspace;
                result = powershell.Invoke();

                if (powershell.HadErrors)
                {
                    StringBuilder strError = new StringBuilder();
                    PSDataCollection<ErrorRecord> errors = powershell.Streams.Error;
                    foreach(ErrorRecord err in errors)
                    {
                        strError.AppendLine(err.ToString());
                    }
                    throw new Exception("Powershell Error: " + strError.ToString());
                }
            }
            return result;
        }
        #endregion
    }
}